<?php include "includes/db_connection.php"; ?>
<?php include "includes/show_veille.php"; ?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>allveilles</title>
    <hearder>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
        <link rel="stylesheet" href="assets/css/styles.css">
    </hearder>
</head>

<body class="body-color">

<?php
    include "includes/navbar_public.php";
?>

    <div class="container">
        <div class="row">
            <?php 
            for ($i=0; $i < sizeof($veilleRss); $i++) { 
            ?>
                <div class="col-12 col-lg-3">
                    <div class="cardBox mt-5 p-3" style="background: url(<?php echo $veilleRss[$i]["image"] ?>) center / cover">
                        <div class="card-footer rounded mt-3">
                            <p class="rounded mt-5 p-2 text-white"><?php echo $veilleRss[$i]["date"] ?></p>
                            <div>
                                <?php echo utf8_encode($veilleRss[$i]["subject"]); ?>
                            </div>
                            <a class="btn btn-info mt-3" href="veille.php?id=<?php echo $veilleRss[$i]["id"]?>">Consulter</a>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
</body>
</html>
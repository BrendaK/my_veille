<?php include "includes/db_connection.php"?>
<?php include "includes/show_veille.php"?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <hearder>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
        <link rel="stylesheet" type="text/css" href="assets/css/styles.css">
    </hearder>
</head>
<style>
    .cardBox {
        height: 650px;
        position: relative;
        overflow: hidden;
        border-radius: 2%;
    }

    .cardBoxMid {
        height: 300px;
        position: relative;
        overflow: hidden;
        border-radius: 2%;
    }

    .cardFooterMid {
        position: absolute;
        width: 80%;
        padding: 10px;
        bottom: -300px;
        left: 10%;
        background: rgba(0, 0, 0, 0.8);
        transition: all 0.5s ease;
        color: white;
        text-align: center;
    }

    .cardBoxMid:hover .cardFooterMid {
        bottom: 20px;
    }
</style>
<body>

<?php include "includes/navbar.php" ?>

          <?php 
    
          for ($i=0; $i < count($veille); $i++) { 
              ?>
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-4">
                    <div style="height: 300px;">
                        <div class="cardBoxMid" style="background: url<?php echo $veille[$i]["image"] ?>)">
                            <h3 class="rounded mt-5 ml-5 mr-5 text-center text-white bg-danger" style="color: rgba(250, 250, 250, 1)"><?php echo $publicBlogResult[1]['title'] ?></h3>
                            <div class="cardFooterMid">
                                <div><?php echo $image[1]['image'] ?></div>
                                <button class="btn btn-danger mt-3" onclick="topic(<?php echo $topic[1]['id'] ?>)">En savoir plus</button>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
              <?php
          }
          
          ?>
</body>
</html>

          <nav class="navbar navbar-expand-lg navbar navbar-dark bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="allveilles.php">Blog</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav ml-lg-auto">
          <li class="nav-item">
            <a class="nav-link" href="allveilles.php">Veilles</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="contact.php">Contact</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="addveille.php" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Ajouter
            </a>
            <ul class="dropdown-menu ml-5" aria-labelledby="navbarDropdownMenuLink">
              <li><a class="dropdown-item" href="addveille.php">Une veille</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
    <script type="text/javascript" src="assets/js/bootstrap.bundle.js"></script>
  </nav>
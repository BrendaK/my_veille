<?php include "includes/db_connection.php"?>
<?php include "includes/show_id_veille.php"?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>veille</title>
    <hearder>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
        <link rel="stylesheet" type="text/css" href="asets/css/styles.css">
    </hearder>
</head>
<body class="body-color">

<?php include "includes/navbar_public.php" ?>

    <div class="container">
    <div class="row mt-5">
        <div class="col-12">
            <div class="card mb-3 text-center">
                <img class="img-veille card-img-top" src="<?php echo $veille["image_url"] ?>" alt="...">
                    <p class="card-text"><small class="text-muted"><?php echo $veille["date"] ?></small></p>
                <div class="card-body">
                    <h5 class="card-title"><?php echo utf8ize($veille["topic"]) ?></h5>
                    <p class="card-text"><?php echo utf8ize($veille["synthesis"]) ?></p>
                    <p class="card-text"><?php echo utf8ize($veille["comment"]) ?></p>
                    <a class="btn btn-primary mt-3" href="allveilles.php">Retour</a>
                    <a class="btn btn-info mt-3 ml-3" type="button" href="<?php echo $veille["link"] ?>">En savoir plus</a>
                </div>
            </div>
        </div>
    </div>
    </div>
</body>
</html>

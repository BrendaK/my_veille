<!DOCTYPE html>
<html lang="fr">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
    integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

  <link rel="stylesheet" href="assets/css/addveille.css">
  <title>ajout-veille</title>
</head>

<body class="linear-gradient">

<?php include "includes/navbar.php" ?>

    <!--formulaire-->
    <form style="margin-top: 2%;">
      <legend class="text-center">Ajouter une veille</legend>
      <div>
        <label for="Date"> Date:</label>
        <input type="date" id="date" name="date">
      </div>
      <div>
        <label for="link"> Lien:</label>
        <input type="link" id="liens" name="link">
      </div>
      <div>
        <label for="Sujet"> Sujet:</label>
        <input type="text" id="sujet" name="Sujet">
      </div>
      <div>
        <label for="Synthèse"> Synthèse:</label>
        <input type="text" id="synthese" name="Synthèse">
      </div>
      <div>
        <label for="Commentaire"> Commentaire:</label>
        <input type="text" id="commentaire" name="Commentaire">
      </div>
      <div>
        <label for="url"> Image (url):</label>
        <input type="url" id="image" name="image">
      </div>

      <div class="button-submit mt-3 text-center">
        <button id="btn-valider" class="btn btn-primary">Valider</button>
      </div>
  </form>
  </div>
</div>

  <!-- Optional JavaScript; choose one of the two! -->

  <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous">
    </script>

  <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
-->

  <script src="../first-app/script/airtable.browser.js"></script>
  <script src="../first-app/script/javascript.js"></script>
</body>

</html>
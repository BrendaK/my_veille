<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>contact</title>
    <hearder>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
        <link rel="stylesheet" type="text/css" href="styles.css">
    </hearder>
</head>
<body>

<?php include "includes/navbar_public.php" ?>
<div class="container">
    <div class="col-12 mt-5">
        <div class="card mb-3 text-center mt-5" style="max-width: 1200px;">
            <div class="row g-0">
                <div class="col-md-4">
                <img class="w-100" style="border-radius: 10%" src="./assets/img/profil.jpg" alt="profil">
                </div>
                <div class="col-md-8">
                <div class="card-body">
                    <h3 class="card-title">KONG Brenda</h3>
                    <p class="card-text mt-3">Profession: Blogueuse</p>
                    <p class="card-text">Adresse: Papeete</p>
                    <p class="card-text">Tél: 87245588</p>
                    <p class="card-text">email: brendakong26@gmail.com</p>
                </div>
                </div>
            </div>
        </div>
    </div>

    <div class="text-center mt-5" style="background-color: pink; border-radius: 10%">
        <p>Vous aimez écrire sur un sujet précis, un centre d’intérêt, une passion dévorante que vous voulez partager avec les autres et faire de cette activité votre métier ? Pas de doute, vous êtes fait pour être blogueur professionnel !</p>
    </div>

<h1 class="text-center mt-5">Compétences</h1>
    <div class="row">
    <div class="col-12 col-lg-3 mt-5">
    <img class="w-100" style="border-radius: 100%" src="./assets/img/cms.jpg" alt="profil">
    <h4 class="text-center mt-3">Gestion de sites web par CSM</h4>
    </div>
    <div class="col-12 col-lg-3 mt-5">
    <img class="w-100" style="border-radius: 100%" src="./assets/img/analytics.jpg" alt="profil">
    <h4 class="text-center mt-3">Analyse des données</h4>
    </div>
    <div class="col-12 col-lg-3 mt-5">
    <img class="w-100" style="border-radius: 100%" src="./assets/img/design.jpg" alt="profil">
    <h4 class="text-center mt-3">Création de visuels</h4>
    </div>
    <div class="col-12 col-lg-3 mt-5">
    <img class="w-100" style="border-radius: 100%" src="./assets/img/socialmedia.jpg" alt="profil">
    <h4 class="text-center mt-3">Gestion des R-S</h4>
    </div>
    </div>

        <div class="row mt-5">
            <div class="col-12 mt-5">
                <form>
                    <fieldset>
                        <legend class="text-center">Contactez moi pour tout renseignements ou demande de devis</legend>

                        <div class="form-group">
                            <label for="nom">Nom</label>
                            <input type="text" class="form-control" id="nom" placeholder="">
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="message">Message</label>
                            <textarea class="form-control" id="bio" rows="3"></textarea>
                        </div>
                    </fieldset>
                    <button class="btn btn-danger mt-3 mb-5">Envoyer</button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<?php include "includes/user.php" ?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>connexion</title>
    <hearder>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
        <link rel="stylesheet" type="text/css" href="assets/css/connexion.css">    </hearder>
</head>
<body>
    <section>
          <div class="background-img">
            <div class="box">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <div class="content">
                <img src="./assets/img/account.svg" height="125px" width="150px;">
                <h1 class="mb-3 mt-3">Connexion</h1>
                <div>
                    <form action="user.php" method="POST">
                        <div class="mb-3">
                        <input type="email" name="email" id="email" placeholder="Email" required="">
                        </div>
                        <div class="mb-3">
                            <input type="password" name="password" placeholder="Mot de passe" required="">    
                        </div>
                        <button id="btn-submit" type="submit" name="submit" class="btn btn-outline-info">Valider</button>
                    </form>
                </div>
            </div>
            </div>
          </div>
      </section>
</body>
<script type="text/javascript" src="scripts.js"></script>
</html>
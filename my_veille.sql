-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 19 fév. 2021 à 10:35
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `my_veille`
--

-- --------------------------------------------------------

--
-- Structure de la table `technological_surveys`
--

DROP TABLE IF EXISTS `technological_surveys`;
CREATE TABLE IF NOT EXISTS `technological_surveys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `topic` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `synthesis` text,
  `comment` text,
  `updated_at` varchar(45) DEFAULT NULL,
  `deleted_at` varchar(45) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_technological_surveys_users1_idx` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `technological_surveys`
--

INSERT INTO `technological_surveys` (`id`, `date`, `topic`, `link`, `synthesis`, `comment`, `updated_at`, `deleted_at`, `image`, `user_id`) VALUES
(1, '2020-11-03', 'LG s’apprêterait à dévoiler un smartphone « e', 'https://www.presse-citron.net/lg-sappreterait', 'En 2019, LG fait sensation en présentant un téléviseur enroulable. D\'autres marques comme Samsung avaient proposé des smartphones à écrans pliables mais encore jamais avec des écrans enroulables. LG s\'apprête à révolutionner la téléphonie en proposant un ', 'La marque coréenne ferrait un grand pas dans l\'innovation en proposant une technologie encore inexistante sur le marché de la téléphonie mobile.', '', '', 'https://dl.airtable.com/.attachments/ea374901d5dbdc0a80a7da5b435c9dbb/1f1e1c5a/lg-smartphone.jpg', 1),
(2, '2020-11-04', 'Involontairement, une compagnie d’assurance a', 'https://www.presse-citron.net/involontairemen', 'Une société d\'assurance suédoise Folksam a annoncé avoir divulgué accidentellement des données personnelles de près d\'un million de clients après de Google, Facebook et Microsoft via LinkedIn. Folksam a immédiatement demandé aux GAFAM de les supprimer et ', 'La protection des données ne doit pas être pris à la légère car en cas de faute, la société se verra dans l\'obligation de payer une amende pouvant aller jusqu\'à 4% de son CA.', '', '', 'https://dl.airtable.com/.attachments/0c2d4c0f401bd9ee00932bdc2c9c959f/d826c063/hacker.jpg', 1),
(3, '2020-11-05', 'Xiaomi a un plan pour devenir le nouveau roi ', 'https://www.presse-citron.net/xiaomi-a-un-pla', 'Xiaomi a développé un nouveau type de caméra pour les smartphones qui permettrait de capturer 300 % de lumière en plus et la clarté serait améliorée de 20 %. En revanche, certaines données ne sont pas encore connues, comme la qualité du zoom sur ce capteu', 'Le constructeur de la marque  serra amené à se placer comme étant l\'un des concurrents les plus sérieux de Huawei (offrant une qualité d\'image exceptionnelle). ', '', '', 'https://dl.airtable.com/.attachments/6c2e8f44d12640e59ad1a08109315991/502a8154/Une-camra-dveloppe-par-Xiaomi-1-.jpg', 1),
(4, '2020-11-06', 'USA : submergés par la désinformation, les gé', 'https://www.presse-citron.net/usa-submerges-p', 'Facebook, Twitter, et les autres réseaux sociaux modèrent comme ils le peuvent les débordements, mais ils agissent souvent avec du retard. La désinformation est souvent utilisé lors de campagnes présidentielles comme c\'est le cas actuellement avec le camp', 'Les géants devraient modérer le plus rapidement possible les postes pour évider la désinformation. Voir mettre en place un système anti-désinformation à l\'aide de partenariats (entre eux).', '', '', 'https://dl.airtable.com/.attachments/a0f80081df9665b971f3635af9b357b7/b08b079c/kayla-velasquez-6Xjl5-Xq4g4-unsplash-1-.jpg', 1),
(5, '2020-11-09', 'La fintech attire les GAFA. L’Inde réagit par', 'https://www.presse-citron.net/la-fintech-atti', 'La mesure entrera en vigueur en janvier prochain, mais chacun aura deux ans pour s’y conformer. L’Inde vient de prendre une décision forte en matière de paiements dans son pays. Scruté de près par les géants de la tech pour son marché très stratégique, il', 'Bonne initiative de la part de l\'Inde dans la mesure où les transactions des GAFAM ne pourront excéder 30% sur le mois.', '', '', 'https://dl.airtable.com/.attachments/ab1bc7dd538a194d4c5d1eb7897f6894/a26d7420/google-pay.jpg', 1),
(6, '2020-11-10', 'Amazon est dans le viseur de la Commission Eu', 'https://www.presse-citron.net/amazon-est-dans', 'La commission européen accuse Amazone d\'utiliser les données des vendeurs pour optimiser ses propres ventes.', 'Il est important d\'assurer la bonne concurrence et l\'équité entre les vendeurs', '', '', 'https://dl.airtable.com/.attachments/f8051e0c62bb767835d8694e5cbaaa02/349ba10e/Union-Europenne-Commission-Europenne-1-.jpg', 1),
(7, '2020-11-12', 'L’interface d’Instagram change et ajoute deux', 'https://www.presse-citron.net/linterface-dins', 'Instagram nous présente une toute nouvelle interface sur laquelle on peut retrouver un menu spécial shop et son propre espace TikTok', 'Instagram sait s\'adapter au changement et aux nouveautés comme on le constate avec cette nouvel interface', '', '', 'https://dl.airtable.com/.attachments/23a1e4abc3395b001b9773c70dd1e1f1/122c69fb/instagram-affichage-maj.jpg', 1),
(8, '2020-11-13', 'Voici ce qu’Apple pourrait faire pour éviter ', 'https://www.presse-citron.net/voici-ce-quappl', 'Apple développerait une fonctionnalité qui vous suggère des applications tierces à télécharger lorsque vous activez un nouvel iPhone ou un nouvel iPad. Dans le but de laisser une chance à ses concurrents et de ne pas reproduire l\'erreur de Google qui s\'es', 'GAFA fait bien de faire attention car est dans le viseur de la commission européenne.', '', '', 'https://dl.airtable.com/.attachments/f70605e2d6c6447609d369f90cade8ce/d61699f0/test-iphone-12-audio.jpg', 1),
(9, '2020-11-16', 'Le Bitcoin est arrivé sur PayPal, secousse à ', 'https://www.presse-citron.net/le-bitcoin-est-', 'PayPal vient de donner le feu vert aux échanges de crypto-monnaies sur son service. Les clients américains, les premiers concernés, peuvent désormais acheter, vendre et détenir des bitcoins, litecoin, ethereum et bitcoin cash.', 'L\'utilisation de la crypto monnaie tend à être à la tendance donc il est important de suivre son évolution... Ainsi que les mouvements des BC suite à l\'évolution de la crypto monnaie.', '', '', 'https://dl.airtable.com/.attachments/0f895ba01376ea5c2782b345e0a47dd2/be3558e2/bitcoin-cours-novembre-2020.jpg', 1),
(10, '2020-11-17', 'Google Maps : des données en temps réels sur ', 'https://www.presse-citron.net/google-maps-des', 'Google Maps se dote de nouvelles fonctionnalités pour aider les utilisateurs à s’adapter durant la pandémie.', 'Il est intéressant de voir comment Google met en place de nouvelles fonctionnalités pour permettre à ses utilisateurs d\'avoir un quotidien amélioré.', '', '', 'https://dl.airtable.com/.attachments/81fd1960e8574efdcaad5a8d2123bf29/6addb136/Google-Maps-nouvelle-fonctionnalitC3A9.jpg', 1),
(11, '2020-11-18', 'Désinformation : les patrons de Facebook et T', 'https://www.presse-citron.net/desinformation-', 'Les 2 géants ont dû faire face aux questions et remarques très critiques de la part des parlementaires de tous bords au sujet de la modération des contenus sur leurs plateformes en lien avec la dernière campagne électorale.', 'Il est nécessaire de modérer les contenus pour éviter la désinformation...', '', '', 'https://dl.airtable.com/.attachments/2bea5727cd6e8f0cc2da1b690f1b4205/f7bbf9b2/zuckerberg-lobbying.jpg', 1),
(12, '2020-11-19', 'Facebook est accusé de forcer ses équipes à r', 'https://www.presse-citron.net/facebook-est-ac', 'Certains modérateurs de Facebook se retrouvent dans l\'obligation de se déplacer au bureau en cette période de pandémie. Même si Facebook affirme favoriser le télétravail, des lettres dénonçant la situation tend à prouvé le contraire.', 'Il me semble inadmissible que le géant des RS ne puisse pas pratiquer le télétravail par l\'ensemble des membres du personnel alors que Twitter le fait.', '', '', 'https://dl.airtable.com/.attachments/6376212bc788d453866cdb097c7bb01f/88e300d9/zuckerberg-facebook.jpg', 1),
(13, '2020-11-20', 'Vaccin contre la Covid-19 : Facebook, Google ', 'https://www.presse-citron.net/vaccin-contre-l', 'Ces plateformes font leur maximum pour ne mettre en avant que des faits vérifiés, ou provenant de sources fiables. Mais un autre événement devrait mettre à mal les réseaux sociaux dans cette bataille contre la désinformation : l’arrivée du vaccin contre l', 'Il me semble important que les plateformes puissent éradiquer le plus rapidement possible les fakes news surtout en ces temps de crise sanitaire.', '', '', 'https://dl.airtable.com/.attachments/64d67a6389802b8900a10c728dad994c/be5c1f7b/vaccin-covid.jpg', 1),
(14, '2020-11-23', 'Instagram : bientôt, un programme pour rémuné', 'https://www.presse-citron.net/instagram-bient', 'Instagram envisage de proposer un programme qui permettra aux médias de gagner de l’argent en publiant des vidéos. Actuellement, le réseau social teste déjà un programme inspiré de celui de YouTube pour permettre à des influenceurs de monétiser des vidéos', 'Il serait temps qu\'Instagram monétise ses vidéos comme le fait Facebook et Youtube. ', '', '', 'https://dl.airtable.com/.attachments/55753a584c800640a6b2c30f991145ec/2bf4de0d/instagram-darkmode.jpeg', 1),
(15, '2020-11-24', 'Facebook lance un outil de collecte pour les ', 'https://www.presse-citron.net/facebook-lance-', 'Le nouvel outil drive développé par Facebook permet aux utilisateurs de collecter de la nourriture, des vêtements et d’autres produits de première nécessité pour les personnes en difficulté.', 'Il est dommage que l’outil ne soit disponible qu’en Amérique et en France.', '', '', 'https://dl.airtable.com/.attachments/3443f671c2079a5cda9bb2c8c46f823d/52f345c3/Facebook-2.jpg', 1),
(16, '2020-11-25', 'Electric Days : l’énergie du futur se décline', 'https://www.presse-citron.net/electric-days-l', 'Le groupe EDF souhaite faire découvrir au grand public un aperçu des solutions énergétiques pour un futur plus durable via son site web. Une plateforme serra également mit à disposition pour ceux recherchant un stage. De plus un prix serra attribué au vai', 'La digitalisation de l’événement me semble approprié et pertinente au vue de la situation sanitaire actuelle. ', '', '', 'https://dl.airtable.com/.attachments/0ee96ca364ad2f0bd164db216f769297/badec935/electricdays.png', 1),
(17, '2020-11-26', 'Apple rémunère des influenceurs TikTok pour p', 'https://www.presse-citron.net/apple-remunere-', 'Apple a fait appels à des personnalités reconnues sur TikTok pour promouvoir son iPhone12 mini.', 'La marque fait bien de se rendre visible sur cette plateforme de plus en plus utilisé.', '', '', 'https://dl.airtable.com/.attachments/fd0d26f2bd412a89843484a82664e6d3/cb048399/tiktok-apple.jpg', 1),
(18, '2020-11-27', 'PlayStation Plus Collection : Des joueurs ban', 'https://www.jeuxvideo.com/news/1330994/playst', 'Face à ce catalogue alléchant, de nombreux détenteurs de PS4 ont voulu y avoir accès sans posséder eux-mêmes une PS5. Cependant, Sony s\'est rapidement rendu compte de la supercherie qui transgresse ses conditions d\'utilisation. Le constructeur japonais a ', 'Mesure surprenante de la part du constructeur', '', '', 'https://dl.airtable.com/.attachments/c23191111a3146d22dd5736dbe14e893/6963c07f/playstation-plus-collection.jpg', 1),
(19, '2020-11-30', 'Samsung Galaxy Note : finalement, un dernier ', 'https://www.presse-citron.net/samsung-galaxy-', 'D’après une nouvelle rumeur, Samsung pourrait finalement sortir un Galaxy Note en 2021. Mais il s’agirait du dernier (source non officiel).', 'La marque n\'a pas encore officialisé la nouvelle donc considérer l\'information avec prudence.', '', '', 'https://dl.airtable.com/.attachments/313cef04dfeb85516b7cc55987347ecc/2a539753/test-samsung-galaxy-note-20-spen.jpg', 1),
(20, '2020-12-01', 'Facebook aurait déboursé un milliard de dolla', 'https://www.presse-citron.net/facebook-aurait', 'Facebook veut utiliser le CRM Kustomer afin d’aider les entreprises à gérer leurs relations client via les services de messagerie (dont Messenger et WhatsApp).', 'Les communications entre les entreprises et les clients sont un élément essentiel du modèle économique de Facebook dans le domaine de la messagerie.', '', '', 'https://dl.airtable.com/.attachments/316cd7a6f33c708eb890dd4e3eacbbfc/e8409ef9/Facebook-logo-oeil.jpg', 1),
(21, '2020-12-02', 'Avec son nouveau fil d’actualité, Google Maps', 'https://www.presse-citron.net/avec-son-nouvea', 'Google Maps propose maintenant un fil d’actualités inspiré des réseaux sociaux qui affiche des recommandations.', 'L’arrivée de ce fil d’actualité sur Google Maps devrait donc être une bonne nouvelle pour les établissements qui ont été affectés par le confinement, à condition de maitriser ce nouveau canal de communication.', '', '', 'https://dl.airtable.com/.attachments/edf7e17c168935f5f69931cd8b8ae2b0/2f40fbe8/Le-fil-dactualitC3A9-de-Google-Maps-1.jpg', 1),
(22, '2020-12-03', 'Et si Xiaomi devançait tous ses concurrents e', 'https://www.presse-citron.net/et-si-xiaomi-de', 'D\'après une rumeur, Xiaomi pourait sortir pour le mois de décembre son Mi 11 qui serait le premier smartphone équipé du processeur Snapdragon 888.', 'Ces information ne sont pas officielle donc les considérer avec prudence.', '', '', 'https://dl.airtable.com/.attachments/41ef134297c16baa91beba3cfc5b4aca/7ce68583/test-xiaomi-mi10t-pro-prix.jpg', 1),
(23, '2020-12-04', 'Google Play Music vient de disparaître pour d', 'https://www.presse-citron.net/google-play-mus', 'C\'est officiel, Google play music n\'existe désormais plus', 'Se tourner vers d\'autres plateformes de streaming comme Spotify, Deezer...', '', '', 'https://dl.airtable.com/.attachments/a73abf00c4657b9a208c0e0db9328dbc/3c1ae2ac/playmusic.jpg', 1),
(24, '2020-12-07', 'Google demande des règles claires, sans pour ', 'https://www.presse-citron.net/google-demande-', 'Selon Matt Brittin, il serai dommage de perturber l\'innovation avec des projets de loi...', 'Google collabore avec les autorités pour mettre en place un système qui pourrait convenir à tout le monde.', '', '', 'https://dl.airtable.com/.attachments/0face6fe7d6cae21645bced0f28fd1f4/f26c91dd/mitchell-luo-jz4ca36oJ_M-unsplash.jpg', 1),
(25, '2020-12-08', 'Google Messages, bientôt aussi sécurisé que W', 'https://www.presse-citron.net/google-messages', 'Google Messages se dote de nouvelles fonctionnalités afin que l’app soit en mesure de rivaliser avec les services comme WhatsApp, Messenger, ou encore Telegram.', 'Le chiffrement, une fonctionnalité nécessaire pour rivaliser avec les autres applications de messagerie instantanée', '', '', 'https://dl.airtable.com/.attachments/d8c03eb3584e6b48e57d7390034b294c/52eff184/Ecran-Google-Pixel-5.jpg', 1),
(26, '2020-12-09', 'Apple menace de bannir les apps qui traquent ', 'https://www.presse-citron.net/apple-menace-de', 'Apple s’apprête à appliquer une nouvelle règle qui obligera les développeurs à demander le consentement de l’utilisateur avant d’accéder à l’IDFA, un identifiant qui peut être utilisé pour cibler les publicités. Ceux qui ne respecteront pas cette nouvelle', 'Il est préférable pour l\'utilisateur.', '', '', 'https://dl.airtable.com/.attachments/10a1ac44e840bb4059ffe081ca1f1cee/f77d2a7a/iphone-12-pro-max-autonomie.jpg', 1),
(27, '2020-12-10', 'La CNIL inflige des amendes de 100 millions €', 'https://www.presse-citron.net/la-cnil-inflige', 'La CNIL sanctionne Google et Amazon à cause de leurs cookies les jugeant trop intrusif.', 'La protection des données des utilisateurs est essentielle donc les navigateurs et autres doivent respecter les mesures en vigueur sous peine de sanction.', '', '', 'https://dl.airtable.com/.attachments/d917a43f9344586a8ea43b19e61c718d/042285f7/CafC3A9-laptop-cookies-et-bureau-travail.jpg', 1),
(28, '2020-12-11', 'Google prend des mesures contre les fausses i', 'https://www.presse-citron.net/google-prend-de', 'Google a rejoint Facebook et Twitter pour créer une alliance afin de lutter contre la désinformation sur les vaccins.', 'Débat sur une perspective de vaccin depuis le début de la pandémie donc bien modérer l\'information.', '', '', 'https://dl.airtable.com/.attachments/2996f11d4b70d4f0cb3744266e9a1b99/21449e62/google.jpg', 1),
(29, '2020-12-14', 'Un problème technique chez Google a perturbé ', 'https://www.lemonde.fr/pixels/article/2020/12', 'Problème du système d\'authentification de 45 min par les services Google (YouTube, Gmail...)', 'La duré n\'aura pas duré longtemps et réparti sur que très faiblement sur la carte.', '', '', 'https://dl.airtable.com/.attachments/82fe824e7de95962252e6f6f3c80bdff/01b2d395/009110c_531719280-image-3.png', 1),
(30, '2020-12-15', 'L’application TikTok aussi sur certaines Smar', 'https://www.presse-citron.net/lapplication-ti', 'TikTok est disponible sur les Smart TV Samsung.', 'Ce réseau social est de plus en plus présent dans notre quotidien.', '', '', 'https://dl.airtable.com/.attachments/04bf2cb4ae35e12a2cab4824f3287560/f251a32b/TikTok-Samsung-Smart-TV-2.jpg,https://dl.airtable.com/.attachments/e975e19532d2b443fc55f60e5f4cff7d/dafccbff/TikT', 1),
(31, '2020-12-16', 'Netflix débarque sur un nouveau support dès a', 'https://www.presse-citron.net/netflix-debarqu', 'Netflix arrive sur les Amazon Echo Show 10.', 'Leader sur son marché, Netflix se fait une place dans l\'univers d\'Amazone.', '', '', 'https://dl.airtable.com/.attachments/e3f326ed3de8f8f06f9e855a42a9b4f5/704cfde6/netflix-nouveautes.jpg', 1),
(32, '2020-12-17', 'Zoom : pas de limite de 40 minutes durant les', 'https://www.presse-citron.net/zoom-pas-de-lim', 'Pendant les fêtes de fin d’année, l\'offre gratuite de Zoom n’aura pas de limite, ce qui permettra aux gens d’organiser des visioconférences de plus de 40 minutes, sans avoir à payer un abonnement.', 'Zoom n’a cessé de développer de nouvelles fonctionnalités destinées à renforcer la sécurité et la confidentialité sur son service de visioconférence. Donc super nouvelle également.', '', '', 'https://dl.airtable.com/.attachments/9ef070c1b476b3bf46437bc78eb54256/a6c51029/Zoom.jpg', 1),
(33, '2021-01-11', 'Apple et Hyundai pourraient rapidement signer', 'https://www.presse-citron.net/apple-et-hyunda', 'D\'ici 2022, une version beta d\'un véhicule 100% électrique créé par Apple et Hyundai.', 'Hâte de voir ce projet se concrétiser', '', '', 'https://dl.airtable.com/.attachments/9e8bb753b0d1f033cfb0da089eb3feb0/ca5ec656/apple-car-concept.jpg', 1),
(34, '2021-01-12', 'Twitter annonce la suppression de 70 000 comp', 'https://www.presse-citron.net/twitter-annonce', 'Twitter ne tolère plus les publications des sympathisants de la théorie complotisteet annonce la suppression de compte liées à QAnon.', 'Rien de surprenant quand on sait qu\'en 2020, le réseau social avait supprimé 7000 comptes liés à QAnon.', '', '', 'https://dl.airtable.com/.attachments/174623b6abbc9d1875837e1e7ece72f1/c47364b5/twitter-site.jpg', 1),
(35, '2021-01-13', 'Razer dévoile un masque futuriste avec ventil', 'https://www.presse-citron.net/razer-devoile-u', 'Razer dévoile un concept de masque qui, en plus d’une protection N95, inclut un ventilateur pour optimiser la respirabilité, et un système d’amplification de la voix. Sinon, le design est transparent afin de ne pas cacher les expressions faciales et un éc', 'Pour le moment il ne s\'agit que d\'un concept donc à voir.', '', '', 'https://dl.airtable.com/.attachments/a248bfd9a0159754eed7b9569b9d4dd8/2e60a12f/Project-Hazel-de-Razer.jpg', 1),
(36, '2021-01-14', 'La CNIL sanctionne le ministère de l’Intérieu', 'https://www.presse-citron.net/la-cnil-sanctio', 'Lors du premier confinement, les forces de police ont utilisé des drones pour surveiller la population. La CNIL condamne cette pratique jugée illégale.', 'Au départ, c\'était un bonne idée de faire survoler les drones pour faire tenter de respecter le confinement mais le droit à l\'image n\'est pas respecté avec cette pratique.', '', '', 'https://dl.airtable.com/.attachments/08c25b7608b4b38f5f685da9518da73f/d519663b/Drone-Nice.jpg', 1),
(37, '2021-01-15', 'En 2020, la pandémie permet au secteur des ap', 'https://www.presse-citron.net/en-2020-la-pand', 'La pandémie aura permit une expansion des app plus importante', 'En temps normal ce secteur n\'aura pas eut besoin pour être en hausse...', '', '', 'https://dl.airtable.com/.attachments/a014c63b6846fa585fc733666fff42a0/1d00030a/Applications.jpg', 1),
(38, '2021-01-18', 'Apple dispose désormais d’un prototype de sma', 'https://www.presse-citron.net/apple-dispose-d', 'À son tour, Apple pourrait présenter sa vision du smartphone pliable.', 'Apple ne serait pas le premier à sortir un modèle pliable puisque Samsung, LG... propose déjà des smartphone pliable.', '', '', 'https://dl.airtable.com/.attachments/849a00cb0dd6af1eeaf05da21cf19f66/480ace08/Le-logo-dApple-sur-un-bC3A2timent.jpg', 1),
(39, '2021-01-19', 'Jedi Blue » : au-delà du contrat publicitaire', 'https://www.presse-citron.net/jedi-blue-contr', 'Un contrat publicitaire entre Facebook et Google donne un clair aperçu du problème des GAFA, et des comportements monopolistiques tels que l\'entente concurrentiel...', 'Il faut rappeler que l\'entente concurrentiel est interdite même si le terme prend un autre nom ou autre forme, les conséquences restes les mêmes.', '', '', 'https://dl.airtable.com/.attachments/07742b5553f6782e2767c5889d9f0b4b/aa11a925/google-facebook-jedi-blue.jpg', 1),
(40, '2021-01-20', 'Zoom sur MeWe, le réseau social « anti-Facebo', 'https://www.presse-citron.net/zoom-sur-mewe-l', 'L’audience de la plateforme est en très forte augmentation ces dernières semaines.', 'Le réseau pourrait prochainement adopter une politique plus stricte pour éviter le même cas à l\'avenir.', '', '', 'https://dl.airtable.com/.attachments/29aa4133a462c11308e0e4c8d734e546/9ccc7764/Screenshot_2021-01-20-MeWe-The-best-chat-group-app-with-privacy-you-trust-.png', 1),
(41, '2021-01-21', 'Taxe GAFA : l’administration Biden semble prê', 'https://www.presse-citron.net/taxe-gafa-ladmi', 'Janet Yellen, la future secrétaire au Trésor américain, l’a laissé entendre lors d’une audition au Sénat ce mercredi.', 'Good', '', '', 'https://dl.airtable.com/.attachments/13ee70bb8e2e71f0f36b9df61adb8e89/097ca526/Screenshot_2020-11-23-AmericaE2', 1),
(42, '2021-01-22', 'Ça chauffe pour les mineurs de Bitcoin\n\n', 'https://www.presse-citron.net/ca-chauffe-pour', 'Coopératives et particuliers, un brin bricoleurs, initient une réflexion de taille sur la consommation électrique. Avec la popularité de Bitcoin, leurs anecdotes devraient être prises au sérieux.', 'la cryptographie à l’énergie solaire ou à d’autres énergies renouvelables, puis utiliser la chaleur résiduelle à des fins productives, est le meilleur moyen de s’engager sur une voie plus verte pour l’industrie dans son ensemble ', '', '', 'https://dl.airtable.com/.attachments/a03f67c8f9c82789ff0483a6c34950a5/46044c3f/bitcoin-minage-chauffage-1024x682.jpg', 1),
(43, '2021-01-25', 'Pourquoi la France ne peut pas “rater le vira', 'https://www.presse-citron.net/pourquoi-la-fra', '1,8 milliard d’euros vont être injectés ces cinq prochaines années, pour faire passer la recherche en technologies quantiques de 60 à 200 millions € par an.', 'Il était temps', '', '', 'https://dl.airtable.com/.attachments/cd3ffff3268eee32c19e0a207a37a224/d91ee260/plan-quantique-france.jpg', 1),
(44, '2021-01-26', 'Test Galaxy S21 Ultra : Samsung a trouvé sa f', 'https://www.presse-citron.net/test-samsung-ga', 'Annoncé mi-janvier 2021, le Galaxy S21 Ultra est le plus premium des derniers smartphones haut de gamme de Samsung. ', 'Un super smartphone à un prix raisonnable.', '', '', 'https://dl.airtable.com/.attachments/49650d99ea5b514ce1d8363d798d0aa1/035fcc21/test-galaxy-s21-ultra-prix.jpg', 1),
(45, '2021-01-27', 'Shorts : la réponse de YouTube à TikTok carto', 'https://www.generation-nt.com/youtube-shorts-', 'YouTube Sort est un test beta qui a rencontré un grand succès en Inde avec plus de 3 milliards de vues par jour.', 'Si une version finale est lancé? saura t-elle se faire une place face à TikTok ?', '', '', 'https://dl.airtable.com/.attachments/9626f4265ca335b583d34530e1826350/b4fc164d/youtube-smartphone_0096006401668202.jpg', 1),
(46, '2021-01-28', 'L’administration Biden souhaite reconsidérer ', 'https://www.presse-citron.net/ladministration', 'Bien que la position de l’administration Biden vis-à-vis de la Chine est la même que celle de l’ancienne administration Trump, la Maison Blanche envisage de réexaminer la politique américaine concernant Huawei.', 'Affaire à suivre...', '', '', 'https://dl.airtable.com/.attachments/4d041a13327d8d16b27dd218173f9051/d32ded68/test-huawei-mate-40-pro-ecran.jpg', 1),
(47, '2021-01-29', 'Mark Zuckerberg ne veut pas que la politique ', 'https://www.clubic.com/pro/blog-forum-reseaux', 'Depuis les élections présidentielles américaines de 2016, la politique et la place qu\'elle prend sur Facebook est une grosse épine dans le pied de la plateforme. Après de multiples tentatives pour faire preuve de transparence et redorer son blason, l\'entr', 'Est-ce que la démarche serra vraiment efficace sachant que les internautes ont tendances de suivre les pages qui les interressent.', '', '', 'https://dl.airtable.com/.attachments/68679df1c3d35874b6eb9d23bcda4cbe/86480f92/rawfitmaxwidth1200hash50040a249de70c803d8966c6ba3408c3a87375e8', 1),
(48, '2021-02-01', 'Sur liste noire du département de la Défense ', 'https://www.generation-nt.com/xiaomi-justice-', 'Le fabricant chinois Xiaomi a déposé une plainte en justice qui vise les départements de la Défense et du Trésor des États-Unis. Elle fait suite à la décision de l\'administration américaine d\'inscrire Xiaomi sur une liste noire. Une initiative officialisé', 'Il fallait s\'attendre à une riposte de la marque, à voir ou cela va aboutir.', '', '', 'https://dl.airtable.com/.attachments/37185a2ba8463d66e887899bf5e1e126/2352b41f/xiaomi_04B0000001672047.jpg', 1),
(49, '2021-02-02', 'Google doit verser 3,8 millions de dollars ap', 'https://www.presse-citron.net/google-doit-ver', 'Le département américain du Travail a tranché, Google doit payer.', 'De plus en plus, les voix s’élèvent pour dénoncer cette mentalité sexiste et discriminatoire qui gangrène le secteur de la tech encore beaucoup trop dominé par les hommes.', '', '', 'https://dl.airtable.com/.attachments/9e317a3f65e2ab4e421efa665641b71a/5aa48b3f/mitchell-luo-jz4ca36oJ_M-unsplash.jpg', 1),
(50, '2021-02-03', 'Apple vise 100 000 Apple Car par an et invest', 'https://www.clubic.com/pro/entreprises/apple/', 'L\'Apple Car pourrait être produite par Kia dans des usines situées aux États-Unis. Un volume de 100 000 voitures électriques par an est évoqué.', 'Il est prévu Apple soit en charge des technologies matérielles et logicielles liées à l\'aide à la conduite ou à la conduite autonome, des semiconducteurs, de la batterie...', '', '', 'https://dl.airtable.com/.attachments/476fe6bb5cfeac1cc94a062b8ba34784/d963cd41/raw', 1),
(51, '2021-02-04', 'Ventes sur internet : plus de 110 milliards d', 'https://www.generation-nt.com/ventes-ligne-fe', 'Avec notamment l\'accélération de la numérisation des magasins physiques, l\'e-commerce a progressé de 8,5 % en 2020. Il compte désormais pour 13,4 % du commerce de détail.', 'Cette évolution à la hausse du montant moyen d\'une transaction est à mettre en rapport avec la situation de crise sanitaire. Il n\'avait en effet pas cessé de baisser précédemment, jusqu\'à atteindre son plus bas niveau historique en 2019 à 59 €.', '', '', 'https://dl.airtable.com/.attachments/431ac9099a93285fd2da24e77ec39520/49c2747f/ecommerce_04B0000001662760.jpg', 1),
(52, '2021-02-05', 'YouTube ajoute une nouvelle section dédiée au', 'https://www.clubic.com/pro/entreprises/google', 'Le site d\'hébergement de vidéos vient tout juste de se munir d\'une catégorie inédite entièrement focalisée sur le sport. Un moyen simple et efficace d\'accéder à toutes les disciplines en un clic (ou presque).', 'YouTube Select permet aux annonceurs de cibler plus facilement les contenus populaires pour acheter de la publicité en adéquation avec les centres d\'intérêt de l\'audience des différentes catégories.', '', '', 'https://dl.airtable.com/.attachments/41719648f58ed159e984c1012bce651b/e5deb671/rawfitmaxwidth1200hash088da3f2b1c0328416f1bed7919099cd6330d308', 1),
(53, '2021-02-08', 'Concurrence : voici comment la Chine veut dom', 'https://www.presse-citron.net/concurrence-voi', 'De nouvelles règles sont mises en place pour lutter contre certains abus de position dominante sur les marchés.', 'L’Europe et les Etats-Unis s’apprêtent aussi à réguler les GAFA', '', '', 'https://dl.airtable.com/.attachments/6e634610ae5877947d10a903e723f6a6/3497962b/chine-y-combinator.jpg', 1),
(54, '2021-02-09', 'Facebook renforce sa lutte contre la désinfor', 'https://www.generation-nt.com/facebook-instag', 'Facebook promet de retirer davantage de messages trompeurs et de la désinformation en rapport avec la Covid-19 et les vaccins.', 'Facebook avait déjà annoncé de réprimer la désinformation en rapport avec la Covid et les anti-vaccins. À chaque fois, il ne parvient pas à concrétiser ces annonces.', '', '', 'https://dl.airtable.com/.attachments/7d733b44f921dd47d2c868634b452a18/314c8002/vaccin_04B0000001671834.jpg', 1),
(55, '2021-02-10', 'Dopé par la soif d’actualité des internautes,', 'https://www.presse-citron.net/dope-par-la-soi', 'Le réseau social a dépassé les 192 millions d’utilisateurs', 'Si une version payante était proposée dans l\'avenir, il faudrait resté à l\'affut.', '', '', 'https://dl.airtable.com/.attachments/bcf6d0a58d7dc818c14b10dad2a698e1/c29e62da/twitter-app.jpg', 1),
(56, '2021-02-11', 'Donald Trump banni à vie de Twitter', 'https://www.generation-nt.com/donald-trump-tw', 'Donald Trump ne sera pas en mesure d\'effectuer un retour sur Twitter, même s\'il se représente à l\'élection présidentielle américaine.', 'Twitter est le premier réseau social à avoir banni l\'ancien président, et envisagé de ne pas le réintégrer donc à voir si la plateforme ne revient pas sir ce qui à été dit.', '', '', 'https://dl.airtable.com/.attachments/0c955a53731b99939588eb2651da6711/515e75d4/twitter-trump_0096006401671569.jpg', 1),
(57, '2021-02-12', 'Quel est le VPN le plus rapide ?', 'https://www.01net.com/actualites/quel-est-le-', 'La nécessité de sécuriser sa connexion ou de contourner des restrictions locales sur le contenu ont fait de l’abonnement VPN un complément intéressant pour surfer de manière anonyme en masquant son adresse IP personnelle et en simulant une connexion depui', 'Pour choisir le VPN le plus rapide, il convient de faire attention à plusieurs données. Déjà il peut être intéressant de s’attarder sur les fonctionnalités les plus mises en avant par le service VPN. Certains VPN sont plus axés sur la sécurité et l’anonym', '', '', 'https://dl.airtable.com/.attachments/fd06f242f5a90f6973e5c3f45a8ec650/babff990/8ed3a07095642fd818f9e1537cdcf.jpg', 1),
(58, '2021-02-15', 'Pourquoi Mark Zuckerberg veut « faire du mal ', 'https://www.presse-citron.net/pourquoi-mark-z', 'La mise a jour IOS 14,5 va permettre aux utilisateurs d\'autoriser ou non les entreprises à exploiter leurs données, soit bloquer le tracking. ', 'Apple met la vie privée des utilisateurs au cœur de ses préoccupations, ce qui représente une bonne nouvelle pour nous.', '', '', 'https://dl.airtable.com/.attachments/32bee3ca1e2d6ceb8c44144745cc3df4/6c699d23/mark-zuckerberg.jpg', 1),
(59, '2021-02-16', 'Perseverance sur Mars : Avant de toucher Mars', 'https://www.20minutes.fr/sciences/2974135-202', 'Lancé en juillet dernier depuis Cap Canaveral, Mars 2020, la mission spatiale d’exploration de la planète Mars conduite par la Nasa, touchera jeudi soir au but de son voyage de 480 millions de kilomètres.\nMais il restera encore à relever un défi de taille', ' La distance entre les deux planètes fait que les communications vont mettre un peu plus de onze minutes pour aller de la Terre au robot et inversement. En cas de problème, le temps que le signal nous parvienne, le robot se sera déjà écrasé.', '', '', 'https://dl.airtable.com/.attachments/b3788c4e2eafa60e26cf27019a083ad4/0e36fc9f/310x190_image-synthese-rover-perseverance-', 1),
(60, '2021-02-17', 'Les lignes de code informatique ont désormais', 'https://www.futura-sciences.com/tech/actualit', 'Pour s\'assurer que la connaissance amassée par les développeurs contemporains ne se perde pas avec le temps, une entreprise américaine sauvegarde des milliers de lignes de code informatique. Un patrimoine numérique pour les générations futures confié en p', 'Les logiciels sont aujourd\'hui au cœur de toutes les activités humaines, de la médecine aux loisirs, des communications à l\'agriculture... Il est donc légitime pour Inria de se soucier de la préservation de toute la connaissance liée au logiciel et de la ', '', '', 'https://dl.airtable.com/.attachments/8709b2dd5bd08e7754f827a93e4680ad/ec25e863/c8b2752599_50170331_coffre-github.jpg', 1),
(61, '2021-02-18', 'Voici les 200 nouveaux émojis qui vont débarquer sur iPhone\r\n', 'https://www.presse-citron.net/nouveaux-emojis-ios145-iphone/', 'Si vous utilisez un iPhone, vous serez ravis d’apprendre que la firme de Cupertino est en passe d’ajouter de nouveaux emojis à la liste de ceux que vous pouvez utiliser dans vos messages et vos publications. Ces dernières années, les emojis sont devenus un moyen d’expression important. Et chaque année, de nouveaux symboles permettent d’enrichir ce moyen d’expression.', 'Apple devrait également déployer une mise à jour de son système d’exploitation qui rendra le ciblage publicitaire sur les applications mobiles plus difficile.', '', '', 'https://www.presse-citron.net/app/uploads/2021/02/Emojis-iPhone-iOS14-5.jpg', 1);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `email_verified_at` varchar(45) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted_at` varchar(45) DEFAULT NULL,
  `deleted_by` varchar(45) DEFAULT NULL,
  `user_type_id1` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_users_user_type_idx` (`user_type_id1`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `email_verified_at`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `user_type_id1`) VALUES
(1, 'Kong', 'Brenda', 'brendakong26@gmail.com', 'azerty', 'brendakong26@gmail.com', '2021-02-17', 'Brenda', '2021-02-17', NULL, '', '', 1);

-- --------------------------------------------------------

--
-- Structure de la table `user_types`
--

DROP TABLE IF EXISTS `user_types`;
CREATE TABLE IF NOT EXISTS `user_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `update_at` varchar(45) DEFAULT NULL,
  `deleted_at` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user_types`
--

INSERT INTO `user_types` (`id`, `name`, `created_at`, `update_at`, `deleted_at`) VALUES
(1, 'administrateur', '2021-02-17', '2021-02-17', '');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `technological_surveys`
--
ALTER TABLE `technological_surveys`
  ADD CONSTRAINT `fk_technological_surveys_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_user_type` FOREIGN KEY (`user_type_id1`) REFERENCES `user_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
